
## Installation

### Prérequis

-   [Composer](https://getcomposer.org/) doit être installé sur votre machine.

### Étapes d'installation

1.  **Clonez le dépôt**
    
-   `git clone https://gitlab.com/emma.t974/api-restauration-ifr.git` 
    
-   **Accédez au répertoire du projet**
   
-   `cd api-restauration-ifr` 
    
-   **Installez les dépendances avec Composer**
    
-   `composer install` 
    
-   **Générez les fausses données**

1.  `composer prepare` 
    
    Cette commande peuplera la base de données avec des données fictives pour vous permettre de tester l'API.
    

## Configuration

1.  **Base de données**
    
    -   Configurez les paramètres de la base de données dans le fichier `.env` en copiant le fichier `.env.example` et en ajustant les valeurs.
2.  **Identifiants d'Utilisateur**
    
    -   Dans le répertoire `src/DataFixtures`, vous trouverez un fichier `AppFixtures.php`. Modifiez si nécessaire si vous souhaitez ajouter de nouveau utilisateur.
    
    Première utilisateur
	  email : departement@reunion.re
	  mot de passe: departement
	  
	 Deuxieme utilisateur
	 email : chef@chef.re
	 mot de passe: chef

## Utilisation

1.  **Lancez le serveur Symfony**
    
1.  `symfony server:start` 
    
    L'API sera accessible à l'adresse [http://localhost:8000/api](http://localhost:8000).
    
2.  **Accédez à la documentation API**
    
    Visitez [http://localhost:8000/api](http://localhost:8000/api/doc) pour consulter la documentation de l'API et commencer à l'utiliser.
    

## Contribution

Si vous souhaitez contribuer à ce projet, veuillez suivre le guide de contribution dans le fichier [CONTRIBUTING.md](https://chat.openai.com/c/CONTRIBUTING.md).

## Problèmes Connus

Consultez la liste des problèmes connus dans le fichier [ISSUES.md](https://chat.openai.com/c/ISSUES.md).

## Licence

Ce projet est sous licence [MIT](https://chat.openai.com/c/LICENSE).