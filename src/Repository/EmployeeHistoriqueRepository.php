<?php

namespace App\Repository;

use App\Entity\EmployeeHistorique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EmployeeHistorique>
 *
 * @method EmployeeHistorique|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeHistorique|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeHistorique[]    findAll()
 * @method EmployeeHistorique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeHistoriqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmployeeHistorique::class);
    }

//    /**
//     * @return EmployeeHistorique[] Returns an array of EmployeeHistorique objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EmployeeHistorique
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
