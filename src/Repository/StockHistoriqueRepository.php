<?php

namespace App\Repository;

use App\Entity\StockHistorique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StockHistorique>
 *
 * @method StockHistorique|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockHistorique|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockHistorique[]    findAll()
 * @method StockHistorique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockHistoriqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockHistorique::class);
    }

//    /**
//     * @return StockHistorique[] Returns an array of StockHistorique objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StockHistorique
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
