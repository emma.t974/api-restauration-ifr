<?php

namespace App\DataFixtures;

use ApiPlatform\Api\QueryParameterValidator\Validator\Length;
use App\Entity\Categorie;
use App\Entity\Employee;
use App\Entity\EmployeeHistorique;
use App\Entity\Produit;
use App\Entity\Stock;
use App\Entity\StockHistorique;
use App\Entity\User;
use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;

class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $userDepartement = new User();
        $userChef = new User();

        $userDepartement
            ->setEmail("departement@reunion.re");

        $userDepartement
            ->setPassword(
                $this->userPasswordHasher->hashPassword($userDepartement, "departement")
            );

        $manager->persist($userDepartement);

        $userChef
            ->setEmail("chef@chef.re");

        $userChef
            ->setPassword(
                $this->userPasswordHasher->hashPassword($userChef, "chef")
            );

        $manager->persist($userChef);

        // POUR LE STOCKAGE

        for ($i = 0; $i < rand(5, 10); $i++) {
            $categorie = new Categorie;

            $categorie
                ->setNom($faker->word());

            $manager->persist($categorie);

            // On génère les produits & le stock
            for ($y = 0; $y < rand(10, 30); $y++) {
                $produit = new Produit;

                $produit
                    ->setNom(
                        $faker->word()
                    )
                    ->setCategorie($categorie);

                $manager->persist($produit);

                // On lui génère un stock
                $stock = new Stock;

                $stock
                    ->setProduit($produit)
                    ->setQuantite(rand(0, 20));

                $manager->persist($stock);

                // On lui génère un historique

                for ($z = 0; $z < rand(5, 10); $z++) {
                    $stockHistorique = new StockHistorique;

                    $stockHistorique
                        ->setEtat(rand(1, 5) > 3 ? "Entrée" : "Sortie")
                        ->setStock($stock)
                        ->setDate($this->generateRandomDateTimeImmutable('2023-01-01', '2023-12-31'));

                    $manager->persist($stockHistorique);
                }
            }
        }

        $roles = [
            "Cuisinier",
            "Préparateur",
            "Cantinière"
        ];
        // On génère les employés
        for ($i = 0; $i < rand(5, 10); $i++) {
            $employee = new Employee;

            $employee
                ->setNom($faker->firstName() . " " . $faker->lastName())
                ->setTelephone(123456789)
                ->setAdresse($faker->sentence())
                ->setRole($roles[rand(0, 2)]);

            $manager->persist($employee);

            for ($z = 0; $z < rand(5, 10); $z++) {
                $employeeHistorique = new EmployeeHistorique;

                $employeeHistorique
                    ->setEtat(rand(1, 5) > 3 ? "Entrée" : "Sortie")
                    ->setEmployee($employee)
                    ->setDate($this->generateRandomDateTimeImmutable('2023-01-01', '2023-12-31'));

                $manager->persist($employeeHistorique);
            }
        }

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    private
    function generateRandomDateTimeImmutable($startDate, $endDate)
    {
        // Convertir les chaînes de date en objets DateTimeImmutable
        $startDateTime = new DateTimeImmutable($startDate);
        $endDateTime = new DateTimeImmutable($endDate);

        // Générer un timestamp aléatoire entre les deux dates
        $randomTimestamp = mt_rand($startDateTime->getTimestamp(), $endDateTime->getTimestamp());

        // Créer un objet DateTimeImmutable avec le timestamp aléatoire
        $randomDateTime = $startDateTime->setTimestamp($randomTimestamp);

        // Retourner l'objet DateTimeImmutable
        return $randomDateTime;
    }
}
