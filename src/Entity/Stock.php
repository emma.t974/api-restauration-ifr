<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StockRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read_stock']],
    denormalizationContext: ['groups' => ['write_stock']],
)]
class Stock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_stock', 'write_stock'])]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read_stock', 'read_product', 'write_stock'])]
    private ?Produit $produit = null;

    #[ORM\OneToMany(mappedBy: 'stock', targetEntity: StockHistorique::class)]
    #[Groups(['read_stock', 'write_stock'])]
    private Collection $stockHistoriques;

    #[ORM\Column]
    private ?int $quantite = null;

    public function __construct()
    {
        $this->stockHistoriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(Produit $produit): static
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * @return Collection<int, StockHistorique>
     */
    public function getStockHistoriques(): Collection
    {
        return $this->stockHistoriques;
    }

    public function addStockHistorique(StockHistorique $stockHistorique): static
    {
        if (!$this->stockHistoriques->contains($stockHistorique)) {
            $this->stockHistoriques->add($stockHistorique);
            $stockHistorique->setStock($this);
        }

        return $this;
    }

    public function removeStockHistorique(StockHistorique $stockHistorique): static
    {
        if ($this->stockHistoriques->removeElement($stockHistorique)) {
            // set the owning side to null (unless already changed)
            if ($stockHistorique->getStock() === $this) {
                $stockHistorique->setStock(null);
            }
        }

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): static
    {
        $this->quantite = $quantite;

        return $this;
    }
}
