<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read_employee']],
    denormalizationContext: ['groups' => ['write_employee']],
)]
class Employee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_employee', 'write_employee'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_employee', 'write_employee'])]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_employee', 'write_employee'])]
    private ?string $telephone = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_employee', 'write_employee'])]
    private ?string $adresse = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_employee', 'write_employee'])]
    private ?string $role = null;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: EmployeeHistorique::class)]
    #[Groups(['read_employee', 'write_employee'])]
    private Collection $employeeHistoriques;

    public function __construct()
    {
        $this->employeeHistoriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): static
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): static
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection<int, EmployeeHistorique>
     */
    public function getEmployeeHistoriques(): Collection
    {
        return $this->employeeHistoriques;
    }

    public function addEmployeeHistorique(EmployeeHistorique $employeeHistorique): static
    {
        if (!$this->employeeHistoriques->contains($employeeHistorique)) {
            $this->employeeHistoriques->add($employeeHistorique);
            $employeeHistorique->setEmployee($this);
        }

        return $this;
    }

    public function removeEmployeeHistorique(EmployeeHistorique $employeeHistorique): static
    {
        if ($this->employeeHistoriques->removeElement($employeeHistorique)) {
            // set the owning side to null (unless already changed)
            if ($employeeHistorique->getEmployee() === $this) {
                $employeeHistorique->setEmployee(null);
            }
        }

        return $this;
    }
}
